// https://codelabs.developers.google.com/codelabs/from-java-to-dart/#1

class Bicycle {
  int cadence;
  int _speed = 3;// it is read only
  int get speed => _speed;
  int gear;

  Bicycle(this.cadence, this.gear);

  // Bicycle(int cadence, int speed, int gear) {
  //   this.cadence = cadence;
  //   this.speed = speed;
  //   this.gear = gear;
  // }



  void applyBreak(int decrement){
    _speed -= decrement;
  }

  void speedUp(int increment){
    _speed += increment;
  }
  // @override
  // String toString(){
  //   return 'Bicycle : $speed mph';
  // }

  // Dart supports single or double quotes when specifying strings.
  @override
  String toString() => 'Bicycle: $_speed mph';
}

void main() {
  var bike = Bicycle(2, 1);

  bike.applyBreak(4);
  print(bike.speed);

}

void printValues(Bicycle bike) {
  print(bike._speed);
  print(bike.speed);
  bike._speed = 5;
  print(bike.speed);
  bike.cadence = 6;
  print(bike.cadence);
}
