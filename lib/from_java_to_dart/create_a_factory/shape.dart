import 'dart:math';

Shape shapeFactory(String type){
  if(type == 'circle') return Circle(2);
  if(type == 'square') return Square(2);
  throw 'Can\'t create $type.';
}

abstract class Shape{
  factory Shape(String type){
    if(type == 'circle') return Circle(2);
    if(type == 'square') return Square(2);
    throw "Can't create $type.";
  }
  num get area;
}

class Circle implements Shape {
  final num radius;
  Circle(this.radius);
  num get area => pi * pow(radius, 2);
}

class Square implements Shape {
  final num side;
  Square(this.side);
  num get area => pow(side, 2);
}

main(){
  final circle = Circle(2);
  final square = Square(2);
  print(circle.area);
  print(square.area);

  // Using a top-level function
  final circle1 = shapeFactory('circle');
  final square1 = shapeFactory('square');
  print(circle1.area);
  print(square1.area);

  // Using the factory constructor
  final circle2 = Shape('circle');
  final square2 = Shape('square');
  print(circle2.area);
  print(square2.area);
}











