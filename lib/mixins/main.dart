mixin Agility {
  var speed = 10;
  void sitDown(){
    print('Sitting down...');
  }
}

class Mammal {
  void breathe(){
    print('Breathe in...breathe out...');
  }
}

// Logically Person has less of a strong connection with the mixin
// Logical relation between Person and Mammal is much stronger
// Agility is like a utility function provider
// Many mixins can be added , but only 1 class can be extended
class Person extends Mammal with Agility{
  String name;
  int age;

  Person(this.name, this.age);
}

void main(){
  final pers = Person('Max',30);
  print(pers.name);
  pers.breathe();
  print(pers.speed);
  pers.sitDown();
}