import 'dart:io';

main()  {
  final envVarMap = Platform.environment;
  print('PWD = ${envVarMap["PWD"]}');
  print('LOGNAME = ${envVarMap["LOGNAME"]}');
  print('PATH = ${envVarMap["PATH"]}');

}

void writeToFileWithAppending() {
  final quotes = File('quotes2.txt').openWrite(mode: FileMode.append);

  quotes.write("Don't cry because it's over, ");
  quotes.writeln("smile because");
  quotes.close();
}

void createsAndWritesToAFile() {
   final quotes = File('quotes.txt');
  const stronger = 'That which does not kill';

  quotes.writeAsString(stronger, mode: FileMode.append);
}