import 'package:intro_to_dart_for_java_dev/discount_cards/bronze.dart';
import 'package:intro_to_dart_for_java_dev/discount_cards/card.dart';
import 'package:intl/intl.dart';

class Purchase {
  double purchaseValue;
  Card card;
  double discountValue;
  double totalValue;

  Purchase(double _purchaseValue, Card _card){
    this.purchaseValue = _purchaseValue;
    this.card = _card;
  }

  double getDiscountValue(){
    discountValue = this.purchaseValue * this.card.discountRate;
    return discountValue;
  }

  double getTotalValue(){
    this.totalValue = this.purchaseValue - this.discountValue;
    return this.totalValue;
  }

  @override
  String toString(){
    String builder = '';
    builder += 'Purchase value: \$${purchaseValue.toStringAsFixed(2)}\n';
    builder += 'Discount rate: ${(card.discountRate * 100).toStringAsFixed(1)}%\n';
    builder += 'Discount: \$${this.getDiscountValue().toStringAsFixed(2)}\n';
    builder += 'Total: \$${this.getTotalValue().toStringAsFixed(2)}';
    return builder;
  }
}
