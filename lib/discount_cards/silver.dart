import 'package:intro_to_dart_for_java_dev/discount_cards/card.dart';

class Silver extends Card {
  Silver(double turnover) {
    this.turnover = turnover;
    this.setDiscountRate();
  }

  void setDiscountRate() {
    if (this.turnover > 300) {
      this.discountRate = 0.035;
    } else {
      this.discountRate = 0.02;
    }
  }
}
