import 'package:intro_to_dart_for_java_dev/discount_cards/bronze.dart';
import 'package:intro_to_dart_for_java_dev/discount_cards/card.dart';
import 'package:intro_to_dart_for_java_dev/discount_cards/gold.dart';
import 'package:intro_to_dart_for_java_dev/discount_cards/purchase.dart';
import 'package:intro_to_dart_for_java_dev/discount_cards/silver.dart';

void main() {
  testBronze();
  print('\n');
  testSilver();
  print('\n');
  testGold1500();
}

void testGold1500() {
  double turnover = 1500;
  Card goldCard = Gold(turnover);
  double valueOfPurchase = 1300;
  Purchase purchase = new Purchase(valueOfPurchase, goldCard);

  print(purchase.toString());
}

void testSilver() {
  double turnover = 600;
  Card silverCard = Silver(turnover);
  double valueOfPurchase = 850;
  Purchase purchase = new Purchase(valueOfPurchase, silverCard);

  print(purchase.toString());
}

void testBronze() {
  double turnover = 0;
  Card bronzeCard = new Bronze(turnover);
  double valueOfPurchase = 150;
  Purchase purchase = new Purchase(valueOfPurchase, bronzeCard);

  print(purchase.toString());
}

void testBronzeNegativePurchaseValue() {
  double turnover = 1;
  Card bronzeCard = Bronze(turnover);
  double valueOfPurchase = -150;
  Purchase purchase = new Purchase(valueOfPurchase, bronzeCard);

  print(purchase.toString());
}
