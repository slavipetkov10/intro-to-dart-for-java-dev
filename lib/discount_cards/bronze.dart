import 'package:intro_to_dart_for_java_dev/discount_cards/card.dart';

class Bronze extends Card {
  Bronze(double turnover) {
    this.turnover = turnover;
    this.setDiscountRate();
  }

  void setDiscountRate() {
    if (this.turnover < 100) {
      this.discountRate = 0;
    } else if (this.turnover >= 100 && this.turnover <= 300) {
      this.discountRate = 0.01;
    } else if (this.turnover > 300) {
      this.discountRate = 0.025;
    }
  }
}
