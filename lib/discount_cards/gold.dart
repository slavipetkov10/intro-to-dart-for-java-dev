import 'package:intro_to_dart_for_java_dev/discount_cards/card.dart';

class Gold extends Card {
  Gold(double turnover) {
    this.turnover = turnover;
    this.setDiscountRate();
  }

  void setDiscountRate() {
    this.discountRate = 0.02;
    double turnover = this.turnover;
    double multiplierOfDiscount = turnover / 100;
    if (multiplierOfDiscount >= 8) {
      this.discountRate = 0.1;
    } else {
      double newDiscount = 0.01 * multiplierOfDiscount;
      this.discountRate = this.discountRate + newDiscount;
    }
  }
}
