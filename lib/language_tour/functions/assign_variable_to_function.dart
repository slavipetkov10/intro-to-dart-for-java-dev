void _updateFromConversion(dynamic unitName) {
  print("Unit name: $unitName");
}

class SomeClass {
  void Function(dynamic arg) myFunction;
}

void main() {
  final c = SomeClass()..myFunction = _updateFromConversion;
  print("Created c. Calling its function");
  c.myFunction("foo");
  print("Done");

  final c1 = SomeClass();
  var myFunction = c1.myFunction;
  // myFunction gets the code from _updateFromConversion
  myFunction =  _updateFromConversion;
  print("Created c1. Calling its function");
  myFunction("foo1");
  print("Done1");

}