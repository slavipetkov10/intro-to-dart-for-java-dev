main(){

  print("-------------- assign a function to a varibale -------------------\n");


  var loudify = (msg) => '!!! ${msg.toUpperCase()} !!!';
  // assert(loudify('hello') == '!!! HELLO !!!');

  print(loudify('yy'));


  print("-------------- function as first-class objects -------------------\n");


  void printElement(int element) {
    print(element);
  }

  var list = [1, 2, 3];

// Pass printElement as a parameter.
  list.forEach(printElement);

  print("-------------- default list and map -------------------\n");

  doStuff();
// bold will be true; hidden will be false.
  enableFlags(bold: true);


  var say1 = say('People', 'words');
  print(say1);

  print("-------------- optional third parameter -------------------\n");

  var say11 = say('People', 'words','telephone');
  print(say11);
}

void doStuff(
    {List<int> list = const [1, 2, 3],
      Map<String, String> gifts = const {
        'first': 'paper',
        'second': 'cotton',
        'third': 'leather'
      }}) {
  print('list:  $list');
  print('gifts: $gifts');
}

/// Sets the [bold] and [hidden] flags ...
void enableFlags({bool bold = false, bool hidden = false}) {
  print(bold);
  print(hidden);
}

String say(String from, String msg, [String device]) {
  var result = '$from says $msg';
  if (device != null) {
    result = '$result with a $device';
  }
  return result;
}