void printInteger(int aNumber){
  print('The number is $aNumber'); // Print to console
}

void main(){
  const bar1 = 100;
  const double atm = 1.5 * bar1;

  var foo = const[];
  final bar = const [];
  const baz = [];
  //bar = [42]; // error
  print(foo);
  print(bar);
  print(baz);

  foo = [1,2,3];
  print(foo);


  dynamic name = 'Bob';
  print(name);
}