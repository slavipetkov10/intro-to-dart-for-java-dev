void main(){


  print("-- Constants -------------------------------\n");
  // These work in a const string.
  const aConstNum = 0;
  const aConstBool = true;
  const aConstString = 'a constant string';

// These do NOT work in a const string.
  var aNum = 0;
  var aBool = true;
  var aString = 'a string';
  const aConstList = [1, 2, 3];

  const validConstString = '$aConstNum $aConstBool $aConstString';
  print(validConstString);
  String invalidConstString = '$aNum $aBool $aString $aConstList';
  print(invalidConstString);

  print("-- Strings -------------------------------\n");

  var s1 = 'String '
      'concatenation'
      " works even over line breaks.";
  print(s1 ==
      'String concatenation works even over '
          'line breaks.');

  var s2 = 'The + operator ' + 'works, as well.';
  print(s2 == 'The + operator works, as well.');

  print("---------------------------------\n");

  var s = 'string interpolation';

  assert('Dart has $s, which is very handy.' ==
      'Dart has string interpolation, ' +
          'which is very handy.');
  assert('That deserves all caps. ' +
      '${s.toUpperCase()} is very handy!' ==
      'That deserves all caps. ' +
          'STRING INTERPOLATION is very handy!');
  print('Dart has $s, which is very handy.' ==
      'Dart has string interpolation, ' +
          'which is very handy.');

  const msPerSecond = 1000;
  const secondsUntilRetry = 5;
  const msUntilRetry = secondsUntilRetry * msPerSecond;

  print(msUntilRetry);
}