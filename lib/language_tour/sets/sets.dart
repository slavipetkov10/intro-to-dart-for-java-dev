main(){


  var name = <String>{};
  name.add("1a");
  name.add("2a");
  name.remove("1a");
  print(name);
  print("-------------- literals -------------------\n");


  var strings = {'1','2',3};
  print(strings);

  print("-------------- literals -------------------\n");
  var halogens = {'fluorine', 'chlorine', 'bromine', 'iodine', 'astatine','astatine'};
  print(halogens);

  print("-------------- literals -------------------\n");


  var elements = <String>{};
  elements.add('fluorine');
  elements.addAll(halogens);
  print(elements);
}