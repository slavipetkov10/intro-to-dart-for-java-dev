import 'dart:collection';

main(){
  HashMap hashMap = new HashMap<int, String>();
  LinkedHashMap linkedHashMap = LinkedHashMap<int, String>();
  SplayTreeMap treeMap = SplayTreeMap<int, String>();

  treeMap[3] = 'c';
  treeMap[1] = 'a';
  treeMap[2] = 'b';

  print(treeMap);

}

void createMapExamples() {
  var gifts = {
    // Key:    Value
    'first': 'partridge',
    'second': 'turtledoves',
    'fifth': 'golden rings'
  };

  gifts["c"] = '3';
  gifts.update('first', (value) => "a");
  var valuesl = gifts.values;
  var keys = gifts.keys;
  var containsKey = gifts.containsKey("first");

  print(keys);
  print(valuesl);

  print(containsKey);

  var nobleGases = {
    2: 'helium',
    10: 'neon',
    18: 'argon',
  };
}