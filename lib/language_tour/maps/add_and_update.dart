main(){
  var gifts = Map();
  gifts['first'] = 'p';
  gifts['second'] = 't';
  gifts['fifth'] = 'go';

  print(gifts);
  gifts['fourth'] = 'cal'; // Add a key-value pair
  print(gifts);

  print(gifts['first'] == 'p');
  gifts['first'] = 't'; // Update a value
  print(gifts);

  var nobleGases = Map();
  nobleGases[2] = 'helium';
  nobleGases[10] = 'neon';
  nobleGases[18] = 'argon';

  bool isNoble(int num){
    return nobleGases[num] != null;
  }

  bool noble = isNoble(2);
  print(noble);

  bool noble1 = isNoble(3);
  print(noble1);

}

