main(){

  print("-------------- Collection for -------------------\n");


  var listOfInts = [1, 2, 3];
  var listOfStrings = [
    '#0',
    for (var i in listOfInts) '#$i'
  ];
  assert(listOfStrings[1] == '#1');
  print(listOfInts);
  print(listOfStrings);

  print("-------------- Collection if -------------------\n");
  bool promoActive = true; // if it is false the   'Outlet' is not added
  var nav = [
    'Home',
    'Furniture',
    'Plants',
    if (promoActive) 'Outlet'
  ];

  print(nav);
  print("-------------- Spread operator -------------------\n");


  var listQ;
  var list2Q = [0, ...?listQ];
  assert(list2Q.length == 1);

  print("-------------- Spread operator -------------------\n");


  var listR = [1, 2, 3];
  var list2 = [0, ...listR];
  assert(list2.length == 4);
  print(list2);

  print("-------------- Strings -------------------\n");

  var listT = [1, 2, 3];
  assert(listT.length == 3);
  assert(listT[1] == 2);

  listT[1] = 1;
  assert(listT[1] == 1);


  print(listT[0]);

  print("-------------- Strings -------------------\n");


  var listS = [
    'Car',
    'Boat',
    'Plane'
  ];
  print(listS);
  print("-------------- Type inference -------------------\n");

  var listN = [1, 2, 3];
  print(listN);
}