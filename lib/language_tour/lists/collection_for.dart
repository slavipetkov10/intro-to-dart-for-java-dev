main(){
  var listOfInts = [1, 2, 3];
  var listOfStrings = [
    '#0',
    for(var i in listOfInts) '#$i'
  ];
  assert(listOfStrings[1] == '#1');

  for (var value in listOfStrings) {
    print(value);
  }
}