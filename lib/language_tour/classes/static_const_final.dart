import 'dart:io';

class Runway
{
  final String item = "Asphalt";
  final List first = ["Asphalt", "Gravel", "Cement"];
  static const List second = ["Asphalt", "Gravel", "Cement"];
  final List third = ["Asphalt", "Gravel", "Cement"];

  List fourth = ["Asphalt", "Gravel", "Cement"];
}

main() {
  Runway rw = new Runway();

  //primitives
  print(rw.item); //prints Asphalt
  //rw.item = "Concrete"; //Error 'item' cannot be used as a setter, its final

  stdout.writeln('Add to a final list');
  rw.first.add('Sand'); //adds 'Sand' to the final List and modifies its value
  for(String l in rw.first){
    stdout.writeln(l);
  }
  //rw.first = ['Steel']; //Error 'first' can't be used as a setter because it's final.

  //Runway.second = ['test']; //Error: Constant variables can't be assigned a value.
  // Runway.second.add('tt'); // Error Cannot add to an unmodifiable list
  // for(String l in Runway.second){
  //   stdout.writeln(l);
  // }

  stdout.writeln('Add to a non-final list');
  rw.fourth.add('Sand1');
  for(String l in rw.fourth){
    stdout.writeln(l);
  }

  stdout.write('change a non final list');
  // change a non final list
  rw.fourth = ['tt','yy'];
  for(String l in rw.fourth){
    stdout.writeln(l);
  }


}