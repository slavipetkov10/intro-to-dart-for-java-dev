class Rectangle {
  double left, top, width, height;

  Rectangle(this.left, this.top, this.width, this.height);

  // Define two calculated properties: right and bottom.
  double get right => left + width;
  // set right(double value) => left = value - width;
  // set right(double value) {
  //   left = value - width;
  // }
  set right(double value) {
    this.right = value;
  }
  double get bottom => top + height;
  set bottom(double value) => top = value - height;
}

void main() {
  //                left top width height
  var rect = Rectangle(3, 4, 20, 15);
  print(rect.left);
  print(rect.top);
  print(rect.width);
  print(rect.height);

  print(rect.right);
  rect.right = 4;
  print(rect.right);
  print(rect.left);

}