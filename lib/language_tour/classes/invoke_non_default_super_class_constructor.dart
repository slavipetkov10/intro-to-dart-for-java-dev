class Person {
  String firstName;

  //Person();
  Person.fromJson(Map data){
    print('in Person');
  }
}

class Employee extends Person {
  //Employee();
  // Person does not have a default constructor
  // I must call super.fromJson(data)
  Employee.fromJson(Map data) : super.fromJson(data){
    print('in Employee');
  }
}

main(){
  var emp = Employee.fromJson({});
  print(emp.runtimeType);
  print(emp);
  //Prints:
  // in Person
  // in Employee
  if(emp is Person){
    //Type check
    emp.firstName = 'Bob';
    print(emp.firstName);
  }
  (emp as Person).firstName = 'Bob1';
  print(emp.firstName);
}
















