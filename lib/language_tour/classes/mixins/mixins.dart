// class Walker{
//   void walk(){
//     print('I\'m walking');
//   }
// }

abstract class Walker {
// This class is intended to be use as a mixin,
// and should not be extended directly

  factory Walker._() => null;

  void walk() {
    print("I'm walking");
  }
}
