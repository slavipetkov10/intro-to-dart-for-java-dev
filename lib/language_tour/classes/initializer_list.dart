class Point{
  final x;
  final y;
  Point.fromJson(Map<String, num> json)
      : x = json['x'],
        y = json['y'] {
    print('In Point.fromJson(): ($x, $y)');
  }

}
class FirstTwoLetters {
  final String letterOne;
  final String letterTwo;

  // Create a constructor with an initializer list here:
  FirstTwoLetters(String word) :
    this.letterOne = word[0],
    this.letterTwo = word[1];

}

main(){
  var f = FirstTwoLetters('test');
  print(f.letterOne);
  print(f.letterTwo);

  initializeAPoint();
}

void initializeAPoint() {
  var map = Map<String, num>();
  map['x'] = 1;
  map['y'] = 2;
  var point = Point.fromJson(map);
  print(point.x);
  print(point.y);
}