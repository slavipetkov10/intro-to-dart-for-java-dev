class Vehicle {
  String make;
  String model;
  int manufactureYear;
  int vehicleAge;
  String color;

  Map<String, dynamic> get map {
    return {
      "make": make,
      "model": model,
      "manufactureYear": manufactureYear,
      "color": color,
    };
  }

  int get age{
    return DateTime.now().year - manufactureYear;
  }

  void set age(int currentYear){
    vehicleAge = currentYear - manufactureYear;
  }
  Vehicle({this.make, this.model, this.manufactureYear, this.color,});
}

main(){
  Vehicle car =
  Vehicle(make:"Honda",model:"Civic",manufactureYear:2010,color:"red");
  print(car.make); // output - Honda
  print(car.model); // output - Civic
  car.age = 2019;
  print(car.age); // output - 9

  var map = car.map;
  print('');
}