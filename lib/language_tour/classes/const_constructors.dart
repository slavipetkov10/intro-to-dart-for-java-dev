class ImmutablePoint{
   const ImmutablePoint(this.x, this.y);

  final int x;
  final int y;

  static const ImmutablePoint origin = ImmutablePoint(0, 0);
  //mutable point
  static  ImmutablePoint origin1 = ImmutablePoint(0, 0);

}

main(){
  var immutablePoint = ImmutablePoint(12, 13);
  print(immutablePoint.x);
  print(immutablePoint.y);
  //immutablePoint.x = 15; //can't be used as a setter because it is final
  print(ImmutablePoint.origin.x);
  //ImmutablePoint.origin = ImmutablePoint(12, 15);// constant variables can't
  // be assigned a value
  print(ImmutablePoint.origin.y);

  ImmutablePoint.origin1 = ImmutablePoint(22, 33); // there is a change
  print(ImmutablePoint.origin1.x);
  print(ImmutablePoint.origin1.y);

}

class Test{
  final int a;
  final int b;
  const Test(this.a,this.b);
}

