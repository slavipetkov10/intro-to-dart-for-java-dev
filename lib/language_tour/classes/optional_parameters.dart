class MyColor{
  int red;
  int green;
  int blue;

  MyColor([this.red = 0, this.green = 0, this.blue = 1]);
  // MyColor({this.red = 0, this.green = 0, this.blue = 0});

}

main(){
  var myColor = MyColor(11, 22);
  // var myColor = MyColor(red: 11, blue: 22);
  print(myColor.red);
  print(myColor.green);
  print(myColor.blue);

}
