void main() {
  dynamic c = get(false);
  print(c.a());
}
dynamic get(bool flag) {
  if (flag) {
    return A();
  } else {
    return B();
  }
}
class A {

  dynamic a() {
    return "a";
  }
}
class B {

  dynamic a() {
    return "b";
  }
}