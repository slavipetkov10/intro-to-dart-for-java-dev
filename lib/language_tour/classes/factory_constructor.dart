import 'package:intro_to_dart_for_java_dev/from_java_to_dart/create_a_factory/shape.dart';

class Square extends Shape {

}

class Circle extends Shape {
  var type = 'circle';
}

class Shape {

  Shape();

  factory Shape.fromTypeName(String typeName) {
    if (typeName == 'square') return Square();
    if (typeName == 'circle') return Circle();

    print('I don\'t recognize $typeName');
    return null;
  }
}

main(){
  var shape = Shape.fromTypeName('square');
  var circleWithNoType = Shape.fromTypeName('circle');
  Circle circle = Shape.fromTypeName('circle');
  Shape circleShape = Shape.fromTypeName('circle');
  var other = Shape.fromTypeName('other');

  print(shape);
  print(circleWithNoType); //no access to type
  print(circle.type);
  print(circleShape);
  print(other);
}
