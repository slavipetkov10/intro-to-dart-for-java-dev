
import 'file:///C:/Users/admin-pc/Desktop/Android_programming/intro_to_dart_for_java_dev/lib/language_tour/classes/private_class/private_fields_methods/a.dart';

void main() {
  A a = new A();
  a.first = 'New first';
 // a._second = 'New second'; // The setter _second is not defined for the class 'A'
  a.second = 'New second'; // access through public setter
  print('${a.first}: ${a.second}'); // access through public getter
 // print('${a.first}: ${a._second}'); // The getter _second is not defined for the class 'A'
  //a._printFields(); //The method '_printFields' isn't defined for the type 'A'.
  a.printFieldsPublic();
}