class A {
  String first;
  String _second;

  String get second {
    return _second;
  }

  void set second(String second) {
    this._second = second;
  }

  void _printFields() {
    print('First: $first, second: $_second');
  }

  void printFieldsPublic() {
    print('First: $first, second: $_second');
  }

}
// https://www.woolha.com/tutorials/dart-using-access-modifiers-private-public
void main() {
  A a = new A();
  a.first = 'New first';
  a._second = 'New second';
  print('${a.first}: ${a._second}');
  a._printFields();
}