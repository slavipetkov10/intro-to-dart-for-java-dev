import '../private_class.dart';

main(List<String> arguments) {
  //var privateClass = _PrivateClass();// cannot create an object of a
  // private class in another package
  final foo = Foo();
  print(foo.privateClass.publicFoo);
  //print(foo.privateClass._privateBar); // invalid because of ._privateBar
  // Cannot access a private field of a private class
}