class Foo {
  final _PrivateClass privateClass = _PrivateClass();
}

class _PrivateClass {
  String publicFoo = 'foo';
  String _privateBar = 'bar';

}

main(){
  var privateClass = _PrivateClass();
  var foo = Foo();
  print(foo.privateClass._privateBar);
}

