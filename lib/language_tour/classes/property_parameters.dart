class MyColor {
  int red;
  int green;
  int blue;

  MyColor({this.red, this.green, this.blue});
}

main(){
  final color = MyColor(red: 80, green: 80, blue: 120);
  print(color.red);
  print(color.green);
  print(color.blue);
}