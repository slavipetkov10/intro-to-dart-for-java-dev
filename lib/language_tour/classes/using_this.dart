class MyColor{
  int red;
  int green;
  int blue;

  MyColor(this.red, this.green, this.blue);
}

main(){
  final color = MyColor(80, 80, 129);
  print(color.red);
  print(color.green);
  print(color.blue);
}