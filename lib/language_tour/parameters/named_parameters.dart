
import 'package:meta/meta.dart';

main(){
  // enableFlags(bold: true,hidden: false);
  // enableFlags(bold: true);
  // enableFlags();
  print('');
  enableVars(second: true);
  enableVars(first: false, second: true);

}

void enableVars({bool first, @required bool second}){
  print(first);
  print(second);
}
void enableFlags({bool bold, bool hidden}){
  print(bold);
  print(hidden);
}