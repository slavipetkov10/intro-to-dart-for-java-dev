void main() {
  future4Errors();
}

void future4Errors() {
  var myFuture = Future(() {});

  print('This runs first!');
  // The second then is executed if there was an error in catchError()
  myFuture.then((result) => print('...'))
      // When Futures return errors we can catch them with catchError()
      // We can catch error thrown by the future or by then() method before the
      // catchError()
      .catchError((error) {
    // Show error to the user
  }).then((_) {
    print('After first then');
  });

  print('This also runs before the future id done!');
}

void future3Chained() {
  var myFuture = Future(() {});

  print('This runs first!');
  // The second then is executed after the first then() is done
  myFuture.then((result) => print('...')).then((_) {
    print('After first then');
  });

  print('This also runs before the future id done!');
}

void future1() {
  var myFuture = Future(() {
    //This code executes immediately
    return 'Hello';
  });
  // Use then() to register a function that will be executed when
  // the Future is done

  print('This runs first!');
  // This function (result) gets the result from the Future
  myFuture.then((result) => print(result));
  print('This also runs before the future id done!');
  // When the other code is done then it goes to the code in the future
  // and if it is done it executed the function in then()
}

void future2() {
  var otherFuture = Future(() {});
  print('Other');
  // If there is nothing returned from the future we should still
  // accept a parameter
  otherFuture.then((_) => print('...'));
  print('Other2');
}
